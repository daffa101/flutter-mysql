import 'package:flutter/material.dart';
import 'package:crud_mysql/editData.dart';
import 'package:http/http.dart' as http;

class Detail extends StatefulWidget{
  final List list;
  final int index;
  Detail({this.list, this.index});

  @override
  _DetailState createState() => new _DetailState();
}

class _DetailState extends State<Detail>{
  
  void deleteData(){
    var url = "http://192.168.43.158:80/my_store/deleteData.php";

    http.post(url, body: {
      'id': widget.list[widget.index]['id']
    });
  }


  void confirm(){
    AlertDialog alertDialog = new AlertDialog(
      title: new Text("WARNING!", style: new TextStyle(color: Colors.yellow),),
      content: new Text("Are You sure want to delete '${widget.list[widget.index]['item_name']}' ?"),
      actions: <Widget>[
        new IconButton(
          icon: Icon(Icons.check),
          color: Colors.green,
          onPressed: (){
            deleteData();
            Navigator.of(context).pushNamedAndRemoveUntil("/Home", 
              (Route<dynamic> route) => false
            );
          },
        ),
        new IconButton(
          icon: Icon(Icons.clear),
          color: Colors.red,
          onPressed: ()=> Navigator.pop(context),
        )
      ],
    );

    showDialog(context: context, child: alertDialog); 
  }
  
  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(title: new Text("${widget.list[widget.index]['item_name']}")),
      body: new Container(
        height: 250.0,
        padding: const EdgeInsets.all(15.0),
        child: new Card(
          child: new Center(
            child: new Column(
              children: <Widget>[
                new Padding(padding: const EdgeInsets.only(top: 20.0),),
                new Text(widget.list[widget.index]['item_name'], style: new TextStyle(fontSize: 18.0),),
                new Text("Code : ${widget.list[widget.index]['item_code']}", style: new TextStyle(fontSize: 16.0),),
                new Text("Harga : ${widget.list[widget.index]['price']}", style: new TextStyle(fontSize: 16.0),),
                new Text("Stock : ${widget.list[widget.index]['stock']}", style: new TextStyle(fontSize: 16.0),),
                new Padding(padding: const EdgeInsets.only(top: 20.0),),

                new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new IconButton(
                      icon: Icon(Icons.edit),
                      iconSize: 40.0,
                      color: Colors.yellow,
                      onPressed: ()=>Navigator.of(context).push(
                        new MaterialPageRoute(
                          builder: (BuildContext context)=>new EditData(list: widget.list, index: widget.index,),
                        )
                      ),
                    ),
                    new IconButton(
                      icon: Icon(Icons.delete),
                      iconSize: 40.0,
                      color: Colors.red,
                      onPressed: ()=> confirm(),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}